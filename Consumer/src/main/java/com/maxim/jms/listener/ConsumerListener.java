package com.maxim.jms.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.maxim.jms.adapter.ConsumerAdapter;

@Component
public class ConsumerListener implements MessageListener {
	
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	ConsumerAdapter consumerAdapter;

	@Override
	public void onMessage(Message message) {
		
		String json = null;
		System.out.println("In onMessge()");
		
		if(message instanceof TextMessage) {
			try {
				json = ((TextMessage)message).getText();
				consumerAdapter.sendToMongo(json);
			} catch (JMSException e) {
				jmsTemplate.convertAndSend(json);
			}
		}
	}

}
